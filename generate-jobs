#!/usr/bin/env python3

import yaml
import os.path
import sys
from itertools import product

# Open the main configuration file
full_document = yaml.safe_load(sys.stdin)

# Extract current CI job
ci_job = full_document[os.getenv('CI_JOB_NAME')]

# Extract job template
job_template = full_document[".template"]

# Select environment variables from the current CI job
# ["A", "B"]
keys = [k for k in ci_job["variables"].keys()] if "variables" in ci_job.keys() and ci_job["variables"] is not None else []

# Split variables values by ","
# [[("A", "1"),  ("A", "2")],   [("B", "3"),  ("B", "4")]]
splitted_variables = [[(k, v) for v in os.environ[k].split(",")] for k in keys]

# Calculate the Cartesian product of variables values
# [(("A", "1"),  ("B", "3")),   (("A", "1"),  ("B", "4")),   (("A", "2"),  ("B", "3")),   (("A", "2"),  ("B", "4"))]
cartesian_product = product(*splitted_variables)

# Store variables values into dictionaries
# [{A:"1", B:"3"},  {A:"1", B:"4"},  {A:"2", B:"3"},  {A:"2", B:"4"}],
environ_dict = [dict(v) for v in cartesian_product]

for i, environ in enumerate(environ_dict, start=1):
    # Encapsulate job template in a job definition
    job_definition = {'matrix-job-{}'.format(i): job_template}
    # Render job definition
    rendered_job_definition = yaml.dump(job_definition)
    # Replace OS variables
    os.environ = environ
    # Expand variables in rendered job definition
    expanded = os.path.expandvars(rendered_job_definition)
    # Print job text
    print(expanded)
